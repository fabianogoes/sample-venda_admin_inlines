from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'vendaproject.views.home', name='home'),
    # url(r'^vendaproject/', include('vendaproject.foo.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(admin.site.urls)),
)
