# coding: utf-8
from django.contrib import admin
from models import *

class VendaProdutosInline(admin.TabularInline):
    model = Venda.produtos.through

class VendaAdmin(admin.ModelAdmin):
    inlines = [
        VendaProdutosInline,
    ]

class ProdutoAdmin(admin.ModelAdmin):
    pass

admin.site.register(Produto, ProdutoAdmin)
admin.site.register(Venda, VendaAdmin)

