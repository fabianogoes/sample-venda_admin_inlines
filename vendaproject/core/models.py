# coding: utf-8
from django.db import models

class Produto(models.Model):
    nome = models.CharField(max_length=100)
    valor = models.FloatField()
    estoque = models.IntegerField()

    def __unicode__(self):
        return self.nome

class Venda(models.Model):
    numero = models.IntegerField()
    valorTotal = models.FloatField(u'Valor Total')
    cliente = models.CharField(max_length=100)
    produtos = models.ManyToManyField(Produto, through='VendaProdutos')

    def __unicode__(self):
        return "%s - %s" % (self.numero, self.valorTotal)

class VendaProdutos(models.Model):
    produto = models.ForeignKey(Produto)
    venda = models.ForeignKey(Venda)
    quantidade = models.IntegerField()
