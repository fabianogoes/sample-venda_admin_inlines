Exemplo de projeto usando Admin com Inlines em relacionamento ManyToMany e through
======

### models.py 
```python
class Produto(models.Model):
    nome = models.CharField(max_length=100)
    ...
```

```python
class Venda(models.Model):
    numero = models.IntegerField()
    produtos = models.ManyToManyField(Produto, through='VendaProdutos')
    ...
```

```python
class VendaProdutos(models.Model):
    produto = models.ForeignKey(Produto)
    venda = models.ForeignKey(Venda)
    quantidade = models.IntegerField()
    ...
```

===

### admin.py

```python
class VendaProdutosInline(admin.TabularInline):
    model = Venda.produtos.through
```    

```python
class VendaAdmin(admin.ModelAdmin):
    inlines = [
        VendaProdutosInline,
    ]
```    

```python
class ProdutoAdmin(admin.ModelAdmin):
    pass
```    

```python
admin.site.register(Produto, ProdutoAdmin)
admin.site.register(Venda, VendaAdmin)
```
